const mongoose = require('mongoose');
const host = process.env.DB_HOST || '127.0.0.1'
// const dbURL = `mongodb://${host}/Loc8r`;
const  dbURL = 'mongodb://hero:HerodaV95@ds241268.mlab.com:41268/heroku_qb449b3x'; 

// if (process.env.NODE_ENV === 'production') {
//     dbURL = process.env.MONGODB_URI; 
//     console.log(`App on production...\nURL is : ${dbURL}`);
// } 

mongoose.connect(dbURL, { useNewUrlParser: true });
mongoose.connection.on('connected', () => {
    console.log(`Mongoose connected to ${dbURL}`);
    console.log('NODE_ENV : ' + process.env.NODE_ENV);
    console.log('URI : ' + dbURL);
});

mongoose.connection.on('error', err => {
    console.log('URI : ' + dbURL);
    console.log(`Mongoose connection error: ${err}`);
});

mongoose.connection.on('disconnected', () => {
    console.log('Mongoose disconnected');
});

const gracefulShutdown = (msg, callback) => {
    mongoose.connection.close(() => {
        console.log(`Mongoose disconnected through ${msg}`);
        callback();
    });
};
// For nodemon restarts
process.once('SIGUSR2', () => {
    gracefulShutdown('nodemon restart', () => {
        process.kill(process.pid, 'SIGUSR2');
    });
});
// For app termination
process.on('SIGINT', () => {
    gracefulShutdown('app termination', () => {
        process.exit(0);
    });
});
// For Heroku app termination
process.on('SIGTERM', () => {
    gracefulShutdown('Heroku app shutdown', () => {
        process.exit(0);
    });
});

require('./locations');
require('./users');