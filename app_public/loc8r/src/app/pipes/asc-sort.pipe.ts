import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ascSort'
})
export class AscSortPipe implements PipeTransform {

  /*Compares dates and returns a number :
    Positive if sorting is needed or negative in the other case */

  private compare(a, b) {
    const createdOnA = a.createdOn;
    const createdOnB = b.createdOn;
    let comparison = 1;
    if (createdOnA > createdOnB) {
      comparison = -1;
    }
    return comparison;
  }

  transform(reviews: any[]): any {
    if (reviews && reviews.length > 0) {
      return reviews.sort(this.compare);
    }
  }

}
