import { Pipe, PipeTransform } from '@angular/core';

/*
  Usage: [innerHTML]='My content... | htmlLineBreaks'
*/

@Pipe({
  name: 'htmlLineBreaks'
})
export class HtmlLineBreaksPipe implements PipeTransform {

  transform(text: string): string {
    return text.replace(/\n/g, '<br/>');
  }

}
