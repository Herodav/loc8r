import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { LocationsListComponent } from './location/locations-list/locations-list.component';
import { LocationDetailsComponent } from './location/location-details/location-details.component';
import { DistancePipe } from './pipes/distance.pipe';
import { HtmlLineBreaksPipe } from './pipes/html-line-breaks.pipe';
import { AscSortPipe } from './pipes/asc-sort.pipe';
import { HttpClientModule } from '@angular/common/http';
import { FrameworkComponent } from './framework/framework.component';
import { AboutComponent } from './about/about.component';
import { HomePageComponent } from './home-page/home-page.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RatingStarsComponent } from './rating-stars/rating-stars.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    LocationsListComponent,
    LocationDetailsComponent,
    DistancePipe,
    HtmlLineBreaksPipe,
    AscSortPipe,
    FrameworkComponent,
    AboutComponent,
    HomePageComponent,
    PageHeaderComponent,
    SidebarComponent,
    RatingStarsComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [FrameworkComponent ]
})
export class AppModule { }
