import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';
import { HistoryService } from '../services/history.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public formError = '';

  public credentials = {
    name: '',
    email: '',
    password: ''
  };

  public pageContent = {
    header: {
      title: 'Sign in to Loc8r',
      strapline: ''
    },
    sidebar: ''
  };
  constructor(
    private authService: AuthService,
    private router: Router,
    private historyService: HistoryService
  ) { }

  ngOnInit() {
  }

  onRegisterSubmit() {
    this.formError = '';
    if (
      !this.credentials.name ||
      !this.credentials.email ||
      !this.credentials.password
    ) {
      this.formError = 'All fields are required, please try again';
    } else {
      this.doRegister();
    }
    console.log('Form submitted');
  }
  private doRegister(): void {
    this.authService.register(this.credentials)
      .subscribe(
        (res) => { this.router.navigateByUrl(this.historyService.getLastNonLoginUrl()); },
        (err) => {
          this.formError = 'Unable to register. Please try again later';
          console.log('Registration error: ' + err.error.message);
        });
  }
}
