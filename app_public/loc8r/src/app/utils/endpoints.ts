import { environment } from '../../environments/environment';

const apibaseUrl = environment.apiBaseUrl;

export const endpoints = {
  locations: apibaseUrl + '/locations?lng=&{lng}&lat=&{lat}&maxDistance=&{maxDistance}',
  locationDetails: apibaseUrl + '/locations/&{id}',
  reviews: apibaseUrl + '/locations/&{id}/reviews',
  login: apibaseUrl + '/login',
  register: apibaseUrl + '/register'
};

