import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-rating-stars',
  templateUrl: './rating-stars.component.html',
  styleUrls: ['./rating-stars.component.scss']
})
export class RatingStarsComponent implements OnInit {

  @Input() rating = 5;
  @Input() editable: false;
  @Output() ratingChange: EventEmitter<number> = new EventEmitter();

  constructor() { }

  rate(index: number) {
    if (!this.editable) {
      return;
    }
    this.rating = index;
    this.ratingChange.emit(this.rating);
  }


  getColor(index: number): string {
    return this.isAboveRating(index) ? 'r' : 's';
  }

  isAboveRating(index: number): boolean {
    return index > this.rating;
  }

  ngOnInit() {
  }

}
