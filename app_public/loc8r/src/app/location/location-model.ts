export class Review {
  author: string;
  rating: number;
  reviewText: string;
  createdOn: Date;
}

export class OpeningTimes {
  days: string;
  opening: string;
  closing: string;
  closed: boolean;
}

export class Location {
  _id: string;
  name: string;
  distance: number;
  address: string;
  rating: number;
  facilities: string[];
  coords: number[];
  openingTimes: OpeningTimes[];
  reviews: Review[];
}
