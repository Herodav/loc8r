import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Loc8rDataService } from 'src/app/services/loc8r-data.service';
import { Location, Review } from '../location-model';
import { RatingStarsComponent } from 'src/app/rating-stars/rating-stars.component';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/services/auth/user-model';
import { UserService } from 'src/app/services/auth/user.service';

@Component({
  selector: 'app-location-details',
  templateUrl: './location-details.component.html',
  styleUrls: ['./location-details.component.scss']
})
export class LocationDetailsComponent implements OnInit {

  @ViewChild('rating', { static: false }) rating: RatingStarsComponent;
  @ViewChild('f', { static: false }) form: NgForm;

  currentUser: User;

  pageContent = {
    header: {
      title: '',
      strapline: ''
    },
    sidebar: ` is on Loc8r because it has accessible wifi and space
    to sit down with your laptop and get some work done.\n\nIf
    you\'ve been and you like it - or if you don\'t - please
    leave a review to help other people just like you.`
  };

  sidebarContent: string;
  currentLocation: Location;
  review: Review;
  imgSrc: string;
  key = 'AIzaSyCSG6nhGIV2IKUaMImqzpq5F7jRxFHl9nk';
  formHidden = true;
  message: string;

  constructor(
    private dataService: Loc8rDataService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getLocation();
    this.authService.isLoggedIn.subscribe(
      (isLoggedIn) => {
        if (isLoggedIn) {
          this.currentUser = this.userService.currentUser;
        } else {
          this.currentUser = null;
        }
      }
    );
  }

  getLocation() {
    this.message = 'Searching for location...';
    const id = this.route.snapshot.paramMap.get('locationId');
    this.dataService.getLocationById(id).subscribe(
      res => {
        this.message = 'Loading...';
        this.currentLocation = res;
        this.pageContent.header.title = this.currentLocation.name;
        this.imgSrc = `https://maps.googleapis.com/maps/api/staticmap?
             center=${this.currentLocation.coords[1]},${this.currentLocation.coords[0]}
             &zoom=14&size=400x350&sensor=false&markers=
             ${this.currentLocation.coords[1]},${this.currentLocation.coords[0]}&key=${this.key}
             `;
        this.message = '';
      });
  }
  onSubmitReview() {
    if (!this.form.valid) {
      return;
    }
    this.message = 'Loading...';
    this.review = {
      author: this.currentUser.name,
      rating: this.onRatingSet(),
      reviewText: this.form.value.review,
      createdOn: new Date()
    };

    this.dataService.addReviewByLocationId(this.currentLocation._id, this.review)
      .subscribe(
        (res) => {
          // console.log(res);
          this.getLocation();
          this.formHidden = true;
          this.message = '';
        },
        (err) => {
          this.message = 'Unable to post the review. Please try again...';
          console.log(err);
        }
      );
  }

  onRatingSet(): number {
    // console.log('rating is: ' + this.rating.rating);
    return this.rating.rating;
  }
  navigateToLogin() {
    this.router.navigateByUrl('/login');
  }


}
