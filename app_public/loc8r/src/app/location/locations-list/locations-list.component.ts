import { Component, OnInit } from '@angular/core';
import { Location } from '../location-model';
import { Loc8rDataService } from 'src/app/services/loc8r-data.service';
import { GeolocationService } from 'src/app/services/geolocation.service';
import { SharedDataService } from 'src/app/services/shared-data.service';

@Component({
  selector: 'app-locations-list',
  templateUrl: './locations-list.component.html',
  styleUrls: ['./locations-list.component.scss']
})
export class LocationsListComponent implements OnInit {

  locations: Location[];
  message: string;
  loading = true;

  constructor(
    private dataService: Loc8rDataService,
    private geolocationService: GeolocationService,
    private sharedDataService: SharedDataService) { }

  ngOnInit() {
    this.getPosition();
  }

  private getLocations(position: any): void {
    this.message = 'Searching for locations...';
    const lat: number = position.coords.latitude;
    const lng: number = position.coords.longitude;
    this.dataService.getLocations(lat, lng).subscribe(
      (foundLoactions) => {
        this.message = 'Loading...';
        this.message = foundLoactions.length > 0 ? '' : 'No locations found';
        this.locations = foundLoactions;
        // console.log(this.locations);
        this.message = '';
      },
      (err) => {
        this.message = 'Oups! An error occured.\nPlease try again...!';
        console.log(err);

      }
    );
  }
  private showError(error: any): void {
    this.message = error.message;
  }

  private noGeo(): void {
    this.message = 'Geolocation not supported by this browser.';
  }

  private getPosition(): void {
    this.message = 'Loading...';
    this.geolocationService.getPosition(
      this.getLocations.bind(this),
      this.showError.bind(this),
      this.noGeo.bind(this));
  }
  getId(location: Location) {
    console.log('id is ' + location._id);
  }

}
