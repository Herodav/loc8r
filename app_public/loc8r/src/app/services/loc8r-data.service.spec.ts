import { TestBed } from '@angular/core/testing';

import { Loc8rDataService } from './loc8r-data.service';

describe('Loc8rDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Loc8rDataService = TestBed.get(Loc8rDataService);
    expect(service).toBeTruthy();
  });
});
