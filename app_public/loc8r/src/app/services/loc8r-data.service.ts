import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Location, Review } from '../location/location-model';
import { endpoints } from '../utils/endpoints';
import { BROWSER_STORAGE } from './auth/storage';

@Injectable({
  providedIn: 'root'
})
export class Loc8rDataService {

  constructor(
    private http: HttpClient,
    @Inject(BROWSER_STORAGE) private storage: Storage
  ) { }

  private baseUrl = endpoints;

  getLocations(lat: number, lng: number): Observable<Location[]> {
    const maxDistance = 200000;
    const url = this.baseUrl.locations
      .replace('&{lng}', lng.toString())
      .replace('&{lat}', lat.toString())
      .replace('&{maxDistance}', maxDistance.toString());
    // console.log('Getting from url: ' + url);
    return this.http.get<Location[]>(url);

  }

  getLocationById(id: string): Observable<Location> {
    const url = `${this.baseUrl.locationDetails.replace('&{id}', id)}`;
    // console.log('Getting from url: ' + url);
    return this.http.get<Location>(url);
  }

  addReviewByLocationId(id: string, review: Review): Observable<Review> {
    const url = `${this.baseUrl.reviews.replace('&{id}', id)}`;
    const httpOptions = {
      headers: new HttpHeaders(
        {
          Authorization: `Bearer ${this.storage.getItem('loc8r-token')}`
        }
      )
    };
    return this.http.post<Review>(url, review, httpOptions);
  }
}
