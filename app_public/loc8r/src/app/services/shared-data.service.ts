import { Injectable } from '@angular/core';
import { BehaviorSubject} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SharedDataService {

  loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor() { }

  set isLoggedIn(loading: boolean) {
    this.loading$.next(loading);

  }
}
