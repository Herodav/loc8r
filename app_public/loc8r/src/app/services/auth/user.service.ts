import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { User } from './user-model';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _currentUser: User;

  constructor() { }

  get currentUser(): User {
    return this._currentUser;
  }

  set currentUser(user: User) {
    this._currentUser = user;
  }

}
