import { Injectable, Inject } from '@angular/core';
import { BROWSER_STORAGE } from './storage';
import { User } from './user-model';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { endpoints } from '../../utils/endpoints';
import { HttpClient } from '@angular/common/http';
import { Authresponse } from './authresponse';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  _isLoggedIn = new BehaviorSubject<boolean>(null);

  constructor(
    @Inject(BROWSER_STORAGE) private storage: Storage,
    private http: HttpClient,
    private userService: UserService
  ) { }

  private baseUrl = endpoints;

  getToken(): string {
    return this.storage.getItem('loc8r-token');
  }

  saveToken(token: string): void {
    this.storage.setItem('loc8r-token', token);
  }

  login(user: User): Observable<any> {
    const url = this.baseUrl.login;
    return this.makeAuthApiCall(url, user);
  }

  register(user: User): Observable<any> {
    const url = this.baseUrl.register;
    return this.makeAuthApiCall(url, user);
  }

  logout() {
    this.storage.removeItem('loc8r-token');
    this._isLoggedIn.next(false);
    // console.log('login state :' + this.isLoggedIn.getValue());
  }

  get isLoggedIn(): BehaviorSubject<boolean> {
    const token = this.getToken();
    if (token) {
      const payload = JSON.parse(atob(token.split('.')[1]));
      this._isLoggedIn.next(payload.exp > (Date.now()) / 1000);
      this.setUSerData();
      // console.log(`${this.userService.currentUser.name} logged in`);
    }
    return this._isLoggedIn;
  }

  private makeAuthApiCall(url: string, user: User): Observable<any> {
    return this.http.post(url, user).pipe(
      tap(
        (res: Authresponse) => {
          this.saveToken(res.token);
          this.isLoggedIn.next(true);
          this.setUSerData();
        },
        (err) => console.log(err)
      ));
  }

  setUSerData() {
    const token = this.storage.getItem('loc8r-token');
    const { email, name } = JSON.parse(atob(token.split('.')[1]));
    this.userService.currentUser = {email, name};
  }
}
