import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { BehaviorSubject } from 'rxjs';
import { UserService } from '../services/auth/user.service';
import { User } from '../services/auth/user-model';

@Component({
  selector: 'app-framework',
  templateUrl: './framework.component.html',
  styleUrls: ['./framework.component.scss']
})
export class FrameworkComponent implements OnInit {

  currentUser: User;

  constructor(
    private authService: AuthService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.authService.isLoggedIn.subscribe(
      (isLoggedIn) => {
        if (isLoggedIn) {
          this.currentUser = this.userService.currentUser;
        } else {
          this.currentUser = null;
        }
      }
    );
  }

  logout(): void {
    this.authService.logout();
  }


}
