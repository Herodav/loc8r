import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';
import { HistoryService } from '../services/history.service';

// both getLastUrl() methods not working....
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public formError = '';

  public credentials = {
    name: '',
    email: '',
    password: ''
  };

  public pageContent = {
    header: {
      title: 'Sign in to Loc8r',
      strapline: ''
    },
    sidebar: ''
  };

  constructor(
    private authService: AuthService,
    private router: Router,
    private historyService: HistoryService) { }

  ngOnInit() {
  }

  onLoginSubmit() {
    this.formError = '';
    if (!this.credentials.email || !this.credentials.password) {
      this.formError = 'All fields are required, please try again';
    } else {
      this.doLogin();
    }
  }

  private doLogin(): void {
    this.authService.login(this.credentials)
      .subscribe(
        (res) => { this.router.navigateByUrl(this.historyService.getPreviousUrl()); },
        (err) => {
          this.formError = err.error.message;
          console.log('Login error: ' + err.error.message);
        });
  }
}
