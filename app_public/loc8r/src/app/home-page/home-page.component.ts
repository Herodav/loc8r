import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  public pageContent = {
    header: {
      title: 'Loc8r',
      strapline: 'Find places to work with wifi near you!'
    },
    sidebar: `Looking for wifi and a seat? Loc8r helps you find places to work when out and about.
    Perhaps with coffee, cake or a pint? Let Loc8r help you find the place you\'re looking for.`
  };

  constructor() { }

  ngOnInit() {
  }

}
