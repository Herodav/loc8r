import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'locations', pathMatch: 'full'
  },
  {
    path: 'locations',
    loadChildren: './locations/locations-list.module#LocationsListPageModule'
  },
  {
    path: 'locations/:locationId',
    loadChildren: './locations/location-details/location-details.module#LocationDetailsPageModule'
  },
  { path: 'about', loadChildren: './about/about.module#AboutPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
