import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { LocationsListPage } from '../locations/locations-list.page';
import { AboutPage } from '../about/about.page';
import { LocationDetailsPage } from '../locations/location-details/location-details.page';
import { RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: LocationsListPage
  },
  {
    path: 'about',
    component: AboutPage
  },
  {
    path: 'locations/:locationId',
    component: LocationDetailsPage
  }
];


// @NgModule({
//   declarations: [],
//   imports: [
//     CommonModule,
//     RouterModule.forRoot(routes)
//   ],
//   exports: [
//     RouterModule
//   ]
// })
// export class AppRoutingModule { }
