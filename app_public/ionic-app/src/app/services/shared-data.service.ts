import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {

  loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor() { }

  set loading(loading: boolean) {
    this.loading$.next(loading);
  }


}
