import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Location, Review } from '../locations/location';
import { environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Loc8rDataService {

  constructor(private http: HttpClient) { }

  // private apiBaseUrl = 'http://localhost:3000/api';

  private apiBaseUrl = environment.apiBaseUrl;

  getLocations(lat: number, lng: number): Observable<Location[]> {
    const maxDistance = 200000;
    const url = `${this.apiBaseUrl}/locations?lng= ${lng}&lat=${lat}&maxDistance=${maxDistance}`;
    console.log('Getting from url: ' + url);
    return this.http.get<Location[]>(url);

  }

  getLocationById(id: string): Observable<Location> {
    const url = `${this.apiBaseUrl}/locations/${id}`;
    console.log('Getting from url: ' + url);
    return this.http.get<Location>(url);
  }

  addReviewByLocationId(id: string, review: Review): Observable<Review> {
    const url = `${this.apiBaseUrl}/locations/${id}/reviews`;
    return this.http.post<Review>(url, review);
  }
}
