import { Component, OnInit } from '@angular/core';
import { Location } from './location';
import { Loc8rDataService } from '../services/loc8r-data.service';
import { GeolocationService } from '../services/geolocation.service';
import { SharedDataService } from '../services/shared-data.service';

@Component({
  selector: 'app-locations-list',
  templateUrl: './locations-list.page.html',
  styleUrls: ['./locations-list.page.scss'],
})
export class LocationsListPage implements OnInit {

  locations: Location[];
  message: string;
  loading = true;

  constructor(
    private dataService: Loc8rDataService,
    private geolocationService: GeolocationService,
    private sharedDataService: SharedDataService) { }

  ngOnInit() {
    this.getPosition();
  }

  private getLocations(position: any): void {
    this.message = 'Searching for locations';
    const lat: number = position.coords.latitude;
    const lng: number = position.coords.longitude;
    this.dataService.getLocations(lat, lng).subscribe(
      (foundLoactions) => {
        this.message = foundLoactions.length > 0 ? '' : 'No locations found';
        this.locations = foundLoactions;
        this.loading = false;
        this.sharedDataService.loading = this.loading;
      }
    );
  }
  private showError(error: any): void {
    this.message = error.message;
  }

  private noGeo(): void {
    this.message = 'Geolocation not supported by this browser.';
  }

  private getPosition(): void {
    this.message = 'Getting your location...';
    this.geolocationService.getPosition(
      this.getLocations.bind(this),
      this.showError.bind(this),
      this.noGeo.bind(this));
  }
  getId(location: Location) {
    console.log('id is ' + location._id);
  }
}
