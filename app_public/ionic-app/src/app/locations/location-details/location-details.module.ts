import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LocationDetailsPage } from './location-details.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: LocationDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  declarations: [LocationDetailsPage, ]
})
export class LocationDetailsPageModule {}
