import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { MapModalComponent } from 'src/app/components/map-modal/map-modal.component';
import { Location, Review } from '../location';
import { Loc8rDataService } from 'src/app/services/loc8r-data.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { RatingComponent } from 'src/app/components/rating/rating.component';
import { NgForm } from '@angular/forms';
import { SharedDataService } from 'src/app/services/shared-data.service';

@Component({
  selector: 'app-location-details',
  templateUrl: './location-details.page.html',
  styleUrls: ['./location-details.page.scss'],
})
export class LocationDetailsPage implements OnInit {

  @ViewChild('rating', { static: false }) rating: RatingComponent;
  @ViewChild('f', { static: false }) form: NgForm;

  sidebarContent: string;
  loc: Location;
  imgSrc: string;
  key = 'AIzaSyCSG6nhGIV2IKUaMImqzpq5F7jRxFHl9nk';
  formHidden = true;
  isLoading = true;

  constructor(
    private modalCtrl: ModalController,
    private dataService: Loc8rDataService,
    private route: ActivatedRoute,
    private sharedDataService: SharedDataService
  ) { }

  ngOnInit() {
    this.getLocation();
  }

  getLocation() {
    this.sharedDataService.loading = this.isLoading;
    const id = this.route.snapshot.paramMap.get('locationId');
    this.dataService.getLocationById(id).subscribe(
      res => {
        this.loc = res;
        this.sidebarContent = `
           ${this.loc.name} is on Loc8r because it has accessible wifi
             and space to sit down with your laptop and get some work done.\n\n
             If you've been there and you like it - or if you don't - please
             leave a review to help other people just like you.
             `;
        this.imgSrc = `https://maps.googleapis.com/maps/api/staticmap?
             center=${this.loc.coords[1]},${this.loc.coords[0]}
             &zoom=14&size=400x350&sensor=false&markers=
             ${this.loc.coords[1]},${this.loc.coords[0]}&key=${this.key}
             `;
        this.isLoading = false;
        this.sharedDataService.loading = false;
      });
  }
  onSubmitReview() {
    if (!this.form.valid) {
      return;
    }
    this.sharedDataService.loading = true;
    const review: Review = {
      author: this.form.value.name,
      rating: this.onRatingSet(),
      reviewText: this.form.value.review,
      createdOn: new Date()
    };

    this.dataService.addReviewByLocationId(this.loc._id, review)
      .subscribe(
        (res) => {
          console.log(res);
          this.sharedDataService.loading = false;
          this.getLocation();
          this.formHidden = true;
        },
        (err) => {
          console.log(err);
          this.sharedDataService.loading = false;
        }
      );
    console.table(review);

  }

  onRatingSet(): number {
    console.log('rating is: ' + this.rating.rating);
    return this.rating.rating;
  }

  onViewLocation() {
    this.modalCtrl.create({
      component: MapModalComponent,
      componentProps: {
        lat: this.loc.coords[1],
        lng: this.loc.coords[0]
      }
    }).then(
      (modalEl) => {
        modalEl.present();
      }
    );
  }

}
