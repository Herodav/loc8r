import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

enum COLORS {
  GREY = '#E0E0E0',
  GREEN = '#13c0cc',
  YELLOW = '#ffcc00',
  RED = '#ad1d28'
}

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
})
export class RatingComponent implements OnInit {


  @Input() rating = 5;
  @Input() editable: false;
  @Output() ratingChange: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit() { }

  rate(index: number) {
    if (!this.editable) {
      return;
    }
    this.rating = index;
    this.ratingChange.emit(this.rating);
  }

  getColor(index: number) {
    if (this.isAboveRating(index)) {
      return COLORS.GREY;
    }
    switch (this.rating) {
      case 1:
      case 2:
        return COLORS.RED;
      case 3:
        return COLORS.YELLOW;
      case 4:
      case 5:
        return COLORS.GREEN;
      default:
        return COLORS.GREY;
    }
  }

  isAboveRating(index: number): boolean {
    return index > this.rating;
  }

}
