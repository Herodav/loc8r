import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RatingComponent } from './rating/rating.component';
import { IonicModule } from '@ionic/angular';
import { MapModalComponent } from './map-modal/map-modal.component';
import { MapViewComponent } from './map-view/map-view.component';
import { AgmCoreModule } from '@agm/core';
import { DistancePipe } from '../pipes/distance.pipe';
import { AscSortPipe } from '../pipes/asc-sort.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';



@NgModule({
  declarations: [
    RatingComponent,
    MapModalComponent,
    MapViewComponent,
    DistancePipe,
    AscSortPipe
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    IonicModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCSG6nhGIV2IKUaMImqzpq5F7jRxFHl9nk'
    })
  ],
  exports: [
    RatingComponent,
    MapModalComponent,
    MapViewComponent,
    AgmCoreModule,
    DistancePipe,
    AscSortPipe
  ],
  entryComponents: [MapModalComponent]
})
export class ComponentsModule { }
