import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MapModalComponent } from '../map-modal/map-modal.component';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss'],
})
export class MapViewComponent implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() { }

  onViewLocation() {
    this.modalCtrl.create({ component: MapModalComponent }).then(
      (modalEl) => {
        modalEl.present();
      }
    );
  }
}
